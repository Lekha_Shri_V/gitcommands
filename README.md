## Getting Started with git

Git is a free and open source version control system, originally created by Linus Torvalds in 2005. 

## Creating a Local Repository: Commands List

- `git init` 
- `git config --local user.name YOUR_NAME`
- `git config --local user.email EMAIL`
- add / change some files
- `git status -s` or `git status`
- `git add FILENAME(S)`
- `git commit -m "COMMIT MSG DESCRIBING CHANGES"`
- `git log --oneline`

## Creating a Remote Repo - on Bitbucket.org

We can use the GUI to perform the following actions that result in creation of a remote repo

- create repo
- add a file
- commit
- view changes